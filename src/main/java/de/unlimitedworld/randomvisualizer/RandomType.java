package de.unlimitedworld.randomvisualizer;

import lombok.Getter;

import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;

public enum RandomType {

	RANDOM_INT("Random", RandomType::getRandomInts, true),
	THREAD_LOCAL_RANDOM("ThreadLocalRandom", RandomType::getThreadLocalRandomInts, false),
	SECURE_RANDOM_INT("SecureRandom", RandomType::secureRandomInts, true),
	XOR_GENERATOR("XOR Generator", RandomType::xorRandom, true);

	@Getter
	private String displayName;

	@Getter
	private BiFunction<String, Integer, RandomGenerator> randomGenrator;

	@Getter
	private boolean allowSeed;

	RandomType(String displayName, BiFunction<String, Integer, RandomGenerator> randomGenrator, boolean allowSeed) {
		this.displayName = displayName;
		this.randomGenrator = randomGenrator;
		this.allowSeed = allowSeed;
	}

	private static RandomGenerator getRandomInts(String seed, int comp) {
		Random random = new Random(seed.hashCode());
		return () -> ((random.nextInt() >> comp) & 1) == 1;
	}

	private static RandomGenerator getThreadLocalRandomInts(String seed, int comp) {
		ThreadLocalRandom random = ThreadLocalRandom.current();
		return () -> ((random.nextInt() >> comp) & 1) == 1;
	}

	private static RandomGenerator secureRandomInts(String seed, int comp) {
		SecureRandom secureRandom = new SecureRandom(seed.getBytes());
		return () -> ((secureRandom.nextInt() >> comp) & 1) == 1;
	}

	private static RandomGenerator xorRandom(String seed, int comp) {
		AtomicInteger xAtomic = new AtomicInteger(seed.hashCode());
		return () -> {
			int x = xAtomic.get();
			x ^= (x << 21);
			x ^= (x >>> 35);
			x ^= (x << 4);
			xAtomic.set(x);
			return ((x >> comp) & 1) == 1;
		};
	}

}
