package de.unlimitedworld.randomvisualizer;

public interface RandomGenerator {

	public boolean nextBoolean();

}
