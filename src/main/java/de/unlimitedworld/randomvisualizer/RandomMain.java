package de.unlimitedworld.randomvisualizer;

import javax.swing.*;

public class RandomMain {

	public static void main(String[] args) {
		MainGUI gui = new MainGUI();
		gui.pack();
		gui.setVisible(true);
		gui.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}

}
