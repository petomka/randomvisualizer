package de.unlimitedworld.randomvisualizer.image;

import java.awt.image.BufferedImage;
import java.util.function.Supplier;

public class ImageGenerator {

	public static BufferedImage generateRandomImage(Supplier<Boolean> randomGenerator, int width, int height) {
		BufferedImage generated = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		for (int y = 0; y < height; y += 2) {
			for (int x = 0; x < width; x += 2) {
				if (randomGenerator.get()) {
					checkAndSet(generated, x, y, 0x000000);
					checkAndSet(generated, x + 1, y, 0x000000);
					checkAndSet(generated, x, y + 1, 0x000000);
					checkAndSet(generated, x + 1, y + 1, 0x000000);
				} else {
					checkAndSet(generated, x, y, 0xFFFFFF);
					checkAndSet(generated, x + 1, y, 0xFFFFFF);
					checkAndSet(generated, x, y + 1, 0xFFFFFF);
					checkAndSet(generated, x + 1, y + 1, 0xFFFFFF);
				}
			}
		}
		generated.flush();
		return generated;
	}

	private static void checkAndSet(BufferedImage image, int x, int y, int color) {
		if (x < 0 || x >= image.getWidth()) {
			return;
		}
		if (y < 0 || y >= image.getHeight()) {
			return;
		}
		image.setRGB(x, y, color);
	}

}
