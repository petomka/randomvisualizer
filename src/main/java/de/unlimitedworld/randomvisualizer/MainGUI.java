/*
 * Created by JFormDesigner on Fri Apr 19 20:33:38 CEST 2019
 */

package de.unlimitedworld.randomvisualizer;

import de.unlimitedworld.randomvisualizer.image.ImageGenerator;
import net.miginfocom.swing.MigLayout;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.function.Supplier;

/**
 * @author Benedikt Karl
 */
public class MainGUI extends JFrame {

	private int randomBitCheck = 0;
	private String seed = "";

	private boolean initialized = false;

	public MainGUI() {
		initComponents();
		this.setVisible(true);
		this.algorithmBox.removeAllItems();
		this.setPreferredSize(new Dimension(750, 560));
		for (RandomType randomType : RandomType.values()) {
			this.algorithmBox.addItem(randomType.getDisplayName());
		}
		InputStream imageInputStream = getClass().getResourceAsStream("/uwsw.png");
		try {
			BufferedImage imBuff = ImageIO.read(imageInputStream);
			this.imageLabel.setIcon(new ImageIcon(imBuff));
		} catch (IOException e) {
			e.printStackTrace();
		}
		initialized = true;
		new Thread(() -> {
			try {
				Thread.sleep(2000);
				recalcImage();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}).start();
	}

	private void generateButtonPressed(ActionEvent e) {
		recalcImage();
	}

	private void recalcImage() {
		this.seed = this.seedField.getText();
		Supplier<Boolean> booleanSupplier = getRandomGenerator(seed);
		BufferedImage image = ImageGenerator.generateRandomImage(booleanSupplier, this.imageLabel.getWidth(),
				this.imageLabel.getHeight());
		this.imageLabel.setIcon(new ImageIcon(image));
	}

	private Supplier<Boolean> getRandomGenerator(String seed) {
		RandomType randomType = getSelectedRandomType();
		if (randomType == null) {
			return null;
		}
		RandomGenerator generator = randomType.getRandomGenrator().apply(seed, randomBitCheck);
		return generator::nextBoolean;
	}

	private RandomType getSelectedRandomType() {
		String algorithm = (String) this.algorithmBox.getSelectedItem();
		for (RandomType randomType : RandomType.values()) {
			if (randomType.getDisplayName().equals(algorithm)) {
				return randomType;
			}
		}
		return null;
	}

	private void bitSliderStateChanged(ChangeEvent e) {
		this.randomBitCheck = this.bitSlider.getValue() - 1;
		this.bitLabel.setText("Bit: " + (randomBitCheck + 1));
		recalcImage();
	}

	private void seedFieldActionPerformed(ActionEvent e) {
		recalcImage();
	}

	private void seedFieldCaretUpdate(CaretEvent e) {
		recalcImage();
	}

	private void algorithmBoxItemStateChanged(ItemEvent e) {
		if (initialized) {
			RandomType randomType = getSelectedRandomType();
			if (randomType != null) {
				this.seedField.setEnabled(randomType.isAllowSeed());
			}
			recalcImage();
		}
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner non-commercial license
		algorithmLabel = new JLabel();
		imageLabel = new JLabel();
		algorithmBox = new JComboBox<>();
		seedLabel = new JLabel();
		seedField = new JTextField();
		bitLabel = new JLabel();
		bitSlider = new JSlider();

		//======== this ========
		setTitle("Zufall? Ich glaube nicht");
		setBackground(Color.darkGray);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setResizable(false);
		setIconImage(new ImageIcon(getClass().getResource("/uwicon.png")).getImage());
		setMinimumSize(new Dimension(16, 32));
		Container contentPane = getContentPane();
		contentPane.setLayout(new MigLayout(
				"hidemode 3",
				// columns
				"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[::190,fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[fill]" +
						"[0,fill]",
				// rows
				"[]" +
						"[]" +
						"[]" +
						"[]" +
						"[]" +
						"[]" +
						"[::20]" +
						"[]" +
						"[]" +
						"[]" +
						"[236:n]" +
						"[]" +
						"[]" +
						"[]" +
						"[]" +
						"[]" +
						"[]" +
						"[]" +
						"[]" +
						"[]" +
						"[]" +
						"[]" +
						"[]" +
						"[]" +
						"[]"));

		//---- algorithmLabel ----
		algorithmLabel.setText("Algorithmus:");
		contentPane.add(algorithmLabel, "cell 0 0");
		contentPane.add(imageLabel, "pad 0,cell 2 0 11 7,wmin 512,hmin 512,gapx 0 0,gapy 0 0");

		//---- algorithmBox ----
		algorithmBox.setModel(new DefaultComboBoxModel<>(new String[]{
				"Random (Integer)",
				"Random (Boolean)",
				"SecureRandom (Integer)",
				"SecureRandom (Boolean)",
				"XORRandom"
		}));
		algorithmBox.addItemListener(e -> algorithmBoxItemStateChanged(e));
		contentPane.add(algorithmBox, "cell 0 1");

		//---- seedLabel ----
		seedLabel.setText("Seed:");
		contentPane.add(seedLabel, "cell 0 3");

		//---- seedField ----
		seedField.setText("Unl1m1t3dW0rld");
		seedField.addActionListener(e -> seedFieldActionPerformed(e));
		seedField.addCaretListener(e -> seedFieldCaretUpdate(e));
		contentPane.add(seedField, "cell 0 4");

		//---- bitLabel ----
		bitLabel.setText("Bit: 1");
		contentPane.add(bitLabel, "cell 0 6");

		//---- bitSlider ----
		bitSlider.setMaximum(32);
		bitSlider.setMinimum(1);
		bitSlider.setValue(1);
		bitSlider.addChangeListener(e -> bitSliderStateChanged(e));
		contentPane.add(bitSlider, "cell 0 7");
		pack();
		setLocationRelativeTo(getOwner());
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner non-commercial license
	private JLabel algorithmLabel;
	private JLabel imageLabel;
	private JComboBox<String> algorithmBox;
	private JLabel seedLabel;
	private JTextField seedField;
	private JLabel bitLabel;
	private JSlider bitSlider;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
